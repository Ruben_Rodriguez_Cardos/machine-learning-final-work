import matplotlib.pyplot as plt
import xml.etree.ElementTree as ET
import sklearn.cluster
import numpy 
from sklearn import preprocessing
from scipy import cluster

import codecs
import pandas as pd, numpy as np, matplotlib.pyplot as plt
import numpy
import csv


#http://docs.scipy.org/doc/scipy/reference/cluster.html
from scipy import cluster
from sklearn import preprocessing 
import sklearn.neighbors
from sklearn.decomposition import PCA


def main():
	#Process
	# 0. Load Data
	f = codecs.open('../Data/TargetData.csv', "r", "utf-8")
	zones = []
	count = 0
	for line in f:
		if count > 0: 
			# remove double quotes
			row = line.replace ('"', '').split(",")
			row.pop(0)
			if row != []:
				data = [int(el) for el in row]
				zones.append(data)
		count += 1

	
	#1. Normalization of the data
	min_max_scaler = preprocessing.MinMaxScaler()
	zones = min_max_scaler.fit_transform(zones)

	# 2. Compute the similarity matrix
	euclidean_dist = sklearn.neighbors.DistanceMetric.get_metric('euclidean')
	matsim = euclidean_dist.pairwise(zones)
	avSim = numpy.average(matsim)
	print("%s\t%6.2f" % ('Distancia Media', avSim))

	# 3. Building the Dendrogram 
	clusters = cluster.hierarchy.linkage(matsim, method = 'ward')
	cluster.hierarchy.dendrogram(clusters, color_threshold=4)
	plt.show()

	clusters = cluster.hierarchy.fcluster(clusters, 3.6, criterion = 'distance')
	print('Number of clusters %d' % (len(set(clusters))))


	reader = csv.reader(open('../Data/TargetData.csv', 'rb'))
	archivo=open ('../Data/TargetDataC.csv',"a")

	x = 0
	y = 0
	for index,row in enumerate(reader):		
		if x == 0:
			for j in range(0,30):
				archivo.write(str(row[j])+",")
			archivo.write("labelJerarquic")
			archivo.write("\n")
			x = x + 1
		else:
			for k in range(0,30):
				archivo.write(str(row[k])+",")
			archivo.write(str(clusters[y]))
			archivo.write("\n")
			y = y + 1

	archivo.close()


if __name__ == "__main__":
	main()