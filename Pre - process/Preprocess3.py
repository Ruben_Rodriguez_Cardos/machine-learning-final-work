# -*- coding: utf-8 -*-

import sqlite3
import os
import csv

print("Cleaning repeated elements...")
try:
	print("Creating database...")
	con = sqlite3.connect('Accidents.db')
	con.text_factory = str
	cursor = con.cursor()

	#CREAMOS LAS TABLAS

	cursor.execute(''' CREATE TABLE Accidents (
		DateOfStop  		varchar(50),
		TimeOfStop			varchar(50),
		Agency 				varchar(50),
		SubAgency			varchar(50),
		Description			varchar(100),
		Location			varchar(50),
		Latitude			varchar(50),
		Longitude			varchar(50),
		Accident			varchar(50),
		Belts				varchar(50),
		PersonalInjury		varchar(50),
		PropertyDamage		varchar(50),
		Fatal				varchar(50),
		CommercialLicense	varchar(50),	
		HAZMAT				varchar(50),
		CommercialVehicle	varchar(50),
		Alcohol				varchar(50),
		WorkZone			varchar(50),
		State				varchar(50),	
		VehicleType			varchar(50),
		Year				integer,
		Make				varchar(50),
		Model				varchar(50),
		Color				varchar(50),
		ViolationType		varchar(50),	
		Charge				varchar(50),
		Article				varchar(50),
		ContributedToAccident	varchar(50),
		Race				varchar(50),
		Gender				varchar(50),
		DriverCity			varchar(50),
		DriverState			varchar(50),
		DLState				varchar(50),
		ArrestType			varchar(50),
		Geolocation			varchar(50)
	);''')
	con.commit()

except sqlite3.OperationalError:
	print("BD already exists")

print("Inserting data in database...")
reader = csv.reader(open('Preproceso2.csv', 'r'))
x = 0
y = 1
for index,row in enumerate(reader):
	if x > 0:
		DateOfStop=str(row[0])
		TimeOfStop=str(row[1])
		Agency=str(row[2])
		SubAgency=str(row[3])
		Description=str(row[4])
		Location=str(row[5])
		Latitude=str(row[6])
		Longitude=str(row[7])
		Accident=str(row[8])
		Belts=str(row[9])
		PersonalInjury=str(row[10])
		PropertyDamage=str(row[11])
		Fatal=str(row[12])
		CommercialLicense=str(row[13])	
		HAZMAT=str(row[14])
		CommercialVehicle=str(row[15])
		Alcohol=str(row[16])
		WorkZone=str(row[17])
		State=str(row[18])	
		VehicleType=str(row[19])
		Year=str(row[20])
		Make=str(row[21])
		Model=str(row[22])
		Color=str(row[23])
		ViolationType=str(row[24])	
		Charge=str(row[25])
		Article=str(row[26])
		ContributedToAccident=str(row[27])
		Race=str(row[28])
		Gender=str(row[29])
		DriverCity=str(row[30])
		DriverState=str(row[31])
		DLState=str(row[32])
		ArrestType=str(row[33])
		Geolocation=str(row[34])

		cursor.execute('''INSERT INTO Accidents (DateOfStop,
				TimeOfStop,
				Agency,
				SubAgency,
				Description,
				Location,
				Latitude,
				Longitude,
				Accident,
				Belts,
				PersonalInjury,
				PropertyDamage,
				Fatal,
				CommercialLicense,
				HAZMAT,
				CommercialVehicle,
				Alcohol,
				WorkZone,
				State,	
				VehicleType,
				Year,
				Make,
				Model,
				Color,
				ViolationType,	
				Charge,
				Article,
				ContributedToAccident,
				Race,
				Gender,
				DriverCity,
				DriverState,
				DLState,
				ArrestType,
				Geolocation) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)''', 
			(DateOfStop, TimeOfStop, Agency, SubAgency, Description, Location, Latitude, Longitude, Accident, Belts, PersonalInjury, PropertyDamage, Fatal, CommercialLicense, HAZMAT, CommercialVehicle, Alcohol, WorkZone, State, VehicleType, Year, Make, Model, Color, ViolationType, Charge, Article, ContributedToAccident, Race, Gender, DriverCity, DriverState, DLState, ArrestType, Geolocation))

	x = x + 1

con.commit()
cursor.close()
con.close()

con = sqlite3.connect('Accidents.db')
cursor = con.cursor()

cursor.execute("SELECT distinct * FROM Accidents")

archivo=open ("BD.csv","a")
archivo.write('''DateOfStop,TimeOfStop,Agency,SubAgency,Description,Location,Latitude,Longitude,Accident,Belts,PersonalInjury,PropertyDamage,Fatal,CommercialLicense,HAZMAT,CommercialVehicle,Alcohol,WorkZone,State,VehicleType,Year,Make,Model,Color,ViolationType,Charge,Article,ContributedToAccident,Race,Gender,DriverCity,DriverState,DLState,ArrestType,Geolocation''')
archivo.write("\n")

for i in cursor:
	try:
		archivo.write(str(i[0]))
		for j in range(1,35):
			archivo.write(","+str(i[j]))
		archivo.write("\n")
	except UnicodeEncodeError:
		pass

archivo.close()
	

con.commit()
cursor.close()
con.close()

#os.system("rm Preprocess2.csv")
#os.system("rm Accidents.db")