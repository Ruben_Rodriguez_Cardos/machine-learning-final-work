Preprocess: Python2

During the preprocess execution, we prepare the data to be used as input on the next stages. The method is what follows:

Preprocess 1: Edit the original csv traffic.cvs in order to clean commas in first place and generating a new csv to be used in Preprocess2.

Preprocess 2: Deleting blanks and errors. An error is defined as a value out of a default range for a column. In addition, a new filter is applied deleting every row wich mark is not like SUBARU (because we are studying SUBARU). Generates a new csv to be used in Preprocess3.

Preprocess 3: Deleting duplicated elements. Generates a new cvs ready to be used in Activity 3.