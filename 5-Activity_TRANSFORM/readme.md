The SacaLabel.py script add a new attribute to each line of the database, the zone that this record, we obtain that zone with a k-means algorithm.

The Activity_5.py script count the amount of each ocurrence from the new attributes that we have defined.

Finally the script PCA.py read the result of the previous script, this script follow this steps: a normalization of the data,a PCA estimation, a K-means clustering and finally show the results in a single figure.

This activity has been made with Python 3.