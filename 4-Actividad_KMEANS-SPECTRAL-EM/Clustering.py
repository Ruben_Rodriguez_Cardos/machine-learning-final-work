import pandas as pd
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
import sklearn.cluster
from sklearn import metrics
from sklearn.datasets.samples_generator import make_blobs
from sklearn.preprocessing import StandardScaler
import sklearn.neighbors
from sklearn.neighbors import kneighbors_graph
import numpy as np


def plotdata(data,labels,name): #def function plotdata
	#colors = ['black']
    fig, ax = plt.subplots()
    plt.scatter([row[0] for row in data], [row[1] for row in data], c=labels)
    ax.grid(True)
    fig.tight_layout()
    plt.title(name)
    
    #Save Result as a Image
    fig1 = plt.gcf()
    plt.show()
    fig1.savefig(name+'.png', dpi=100)

def main():
	df = pd.read_csv('../Data/BD.csv')
	matrix =df.as_matrix(columns=['Geolocation'])
	
	data=[]
	for i in matrix:
		#print(i)
		i=str(i)
		i=i.split(" ")
		data.append([float(i[0][3:]),float(i[2][:len(i[1])-3])])	
	

	#Number of clusters obtained in nClusters.py
	k=13
	init = "k-means++"
	#Calculate and plot the results of k-means
	centroids, labels, z =  sklearn.cluster.k_means(data, k, init)
	plotdata(data,labels, 'K-means')
	print("Silhouette Coefficient (K-means): %0.3f"% metrics.silhouette_score(np.asarray(data), labels))

	#Calculate and plot the results of EM
	from sklearn.mixture import GMM
	classifier = GMM(n_components=13,covariance_type='full', init_params='wc', n_iter=20)
	classifier.fit(data)
	labels =  classifier.predict(data)
	plotdata(data,labels,'EM')
	print("Silhouette Coefficient (EM): %0.3f" % metrics.silhouette_score(np.asarray(data), labels))

	#Calculate and plot the Spectral Clustering results    
	spectral = sklearn.cluster.SpectralClustering(n_clusters=13, eigen_solver='arpack', affinity="nearest_neighbors")
	labels = spectral.fit_predict(data)
	plotdata(data,labels,'Spectral CLustering')
	print("Silhouette Coefficient (Spectral): %0.3f"% metrics.silhouette_score(np.asarray(data), labels))


	

if __name__ == "__main__":
	main()