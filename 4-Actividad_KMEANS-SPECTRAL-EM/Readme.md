In this part of the work, we must decided which algorithm use for clustering.
We have 3 algorithms, EM, K-means and Spectral Clustering. 
We have to calculate the number of clusters used for k-means. In the nCluster.py file, the distorsion and silhouette it's calculated 
for 2 to 20 clusters. The number of clusters we used is 13 because the distortion is low. For more details, see the "distorsion.png" file.
In the images, we can see the different results obtained with each algorithm and the silhouettes are:

K-means:
	-Silhouette Coefficient: 0.406

EM:
	-Silhouette Coefficient: 0.147

Spectral Clustering:
	-Silhouette Coefficient: -0.314


As we see, the best result is the K-means.

This activity has been made with Python 3.