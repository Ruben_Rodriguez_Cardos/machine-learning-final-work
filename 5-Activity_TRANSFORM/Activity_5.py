import pandas as pd
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
import sklearn.cluster
from sklearn import metrics
from sklearn.datasets.samples_generator import make_blobs
from sklearn.preprocessing import StandardScaler
import sklearn.neighbors
from sklearn.neighbors import kneighbors_graph
import numpy as np
import csv

def main():
	print("Hi")
	df = pd.read_csv('../Data/BDC.csv')

	#Result file
	result_file = open ('../Data/TargetData.csv',"a")
	result_file.write("X,NumbersElements,PropertyDamage,Belts,Mujeres,Hombres,GOLD,BLUE  DARK,WHITE,BLUE,YELLOW,BEIGE,BLUE  LIGHT,GREEN  LGT,MULTICOLOR, CREAM,TAN,ORANGE,CHROME,SILVER,BLACK,PURPLE,GREEN  DK,RED,BROWN,GREEN,N/A,MAROON,BRONZE,GRAY")
	result_file.write("\n")	

	for i  in range(0,13): #We have 13 zones
		
		#Dic with all the result data
		dic = {}
		
		dic["Mujeres"]=0
		dic["Hombres"]=0

		#Count Gender
		matrix = df.as_matrix(columns=['Gender','Label'])
		for it in matrix:
			if it[0] == "M" and it[1] == i:
				if 'Mujeres' in dic.keys():
					dic["Mujeres"]+=1
			else:
				if 'Hombres' in dic.keys():
					dic["Hombres"]+=1

		#Count Property Damage
		matrix = df.as_matrix(columns=['PropertyDamage','Label'])
		dic["PropertyDamage"]=0
		for it in matrix:
			if it[0] == "Yes" and it[1] == i:
				if 'PropertyDamage' in dic.keys():
					dic["PropertyDamage"]+=1

		#Count Belts
		matrix = df.as_matrix(columns=['Belts','Label'])
		dic["Belts"]=0
		for it in matrix:
			if it[0] == "No" and it[1] == i:
				if 'Belts' in dic.keys():
					dic["Belts"]+=1

		
		#Distinct Colors
		reader = csv.reader(open('../Data/BDC', 'r'))
		colores = []
		for index,row in enumerate(reader):
			color = str(row[23])
			x = color in colores
			if x == False:
				colores.append(str(color))

		for color in colores:
			dic[color]=0

		#Count Color
		matrix = df.as_matrix(columns=['Color','Label'])
		for it in matrix:
			aux=it[0]
			if it[1] ==i and aux in dic.keys():
				dic[aux]+=1

		#Write a CSV file with the result
		#"X,NumbersElements,PropertyDamage,Belts,Mujeres,Hombres,GOLD,BLUE  DARK,WHITE,BLUE,YELLOW,BEIGE,BLUE  LIGHT,GREEN  LGT,MULTICOLOR, CREAM,TAN,ORANGE,CHROME,SILVER,BLACK,PURPLE,GREEN  DK,RED,BROWN,GREEN,N/A,MAROON,BRONZE,GRAY"
		n_elements=dic["Hombres"]+dic["Mujeres"]
		result_file.write(str(i)+","+str(n_elements)+","+str(dic["PropertyDamage"])+","+str(dic["Belts"])+","+str(dic["Mujeres"])+","+str(dic["Hombres"])+","+str(dic["GOLD"])+","+str(dic["BLUE  DARK"])+","+str(dic["WHITE"])+","+str(dic["BLUE"])+","+str(dic["YELLOW"])+","+str(dic["BEIGE"])+","+str(dic["BLUE  LIGHT"])+","+str(dic["GREEN  LGT"])+","+str(dic["MULTICOLOR"])+","+str(dic["CREAM"])+","+str(dic["TAN"])+","+str(dic["ORANGE"])+","+str(dic["CHROME"])+","+str(dic["SILVER"])+","+str(dic["BLACK"])+","+str(dic["PURPLE"])+","+str(dic["GREEN  DK"])+","+str(dic["RED"])+","+str(dic["BROWN"])+","+str(dic["GREEN"])+","+str(dic["N/A"])+","+str(dic["MAROON"])+","+str(dic["BRONZE"])+","+str(dic["GRAY"]))
		result_file.write("\n")	


		
		#print("Zone: "+str(i))
		#print(dic)
	result_file.close()
	

if __name__=="__main__":
	main()