Activity 7

In this activity we apply the hierarchical clustering algorithm based on the resulting dendrogram, to obtain the DataTarget data classified in groups.

As we can see in the dendrogram, there are clearly 4 well defined groups so we apply the cut with a value of 3.6 units.


Seguidamente aplicamos el algoritmo jerarquico y escribimos el numero de cluster a la que pertenece cada uno en el archivo DataTargetC.csv.