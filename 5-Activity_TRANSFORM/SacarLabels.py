# -*- coding: utf-8 -*-



import pandas as pd, numpy as np, matplotlib.pyplot as plt
import sklearn.cluster
from sklearn import metrics
from sklearn.cluster import KMeans
import csv


#Load data (latitude and longitude) of the province of Araba filtered by type: accident
#load Data of Geolocation
df = pd.read_csv('../Data/BD.csv')
matrix =df.as_matrix(columns=['Geolocation'])

data=[]
for i in matrix:
	#print(i)
	i=str(i)
	i=i.split(" ")
	data.append([float(i[0][3:]),float(i[2][:len(i[1])-3])])

reader = csv.reader(open('../Data/BD.csv', 'rb'))
archivo=open ('../Data/BDC.csv',"a")

init = 'random' # initialization method 
iterations = 10 # to run 10 times with different random centroids to choose the final model as the one with the lowest SSE
max_iter = 300 # maximum number of iterations for each single run
tol = 1e-04 # controls the tolerance with regard to the changes in the within-cluster sum-squared-error to declare convergence
random_state = 0 # random

k = 13 # The K is chosen in activity 4 since it has the best coefficient of Silohouette

km = KMeans(13, init, n_init = iterations ,max_iter= max_iter, tol = tol,random_state = random_state)
labels = km.fit_predict(data)

x = 0
y = 0
for index,row in enumerate(reader):
	if x == 0:
		for i in range(35):
			archivo.write(str(row[i])+",")
		archivo.write("Label")
		archivo.write("\n")
		x = x + 1
	else:
		for i in range(35):
			archivo.write(str(row[i])+",")
		archivo.write(str(labels[y]))
		archivo.write("\n")
		y = y +1


archivo.close()